<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');



/****[Module Products]****/

  /*
   * Desarrollamos las rutas para el modulo de registro y login de usuarios
   */

  Route::POST('users/login/','Users\Api\UserController@login')->name('api.users.login');
  Route::POST('users/signup/','Users\Api\UserController@signup')->name('api.users.signup');
  
  
  /*
   * Desarrollamos las rutas para el modulo de paises (CountryController)
   */

  Route::GET('country/', 'Country\Api\CountryController@getCountryList')->name('api.country.getCountryList');
  
  /*
   * Desarrollamos las rutas para el modulo de estados (StateController)
   */
  
  Route::GET('/state/{id_state_country}', 'State\Api\StateController@getStateList')->name('api.state.getStateList');
  
  
 /*
   * Desarrollamos las rutas para el modulo de ciudades (CityController)
   */
  
  Route::GET('/city/{id_city_state}', 'City\Api\CityController@getCityList')->name('api.city.getCityList');

//categorys
  Route::GET('products/category/', 'Products\Api\ProductCategoryController@index')->name('api.products.category.index');
  Route::POST('products/category/','Products\Api\ProductCategoryController@store')->name('api.products.category.store');
  Route::GET('products/category/{id}', 'Products\Api\ProductCategoryController@show')->name('api.products.category.show');
  Route::DELETE('products/category/{id}','Products\Api\ProductCategoryController@destroy')->name('api.products.category.destroy');
  Route::PUT('products/category/{id}','Products\Api\ProductCategoryController@update')->name('api.products.category.update');

	//Product
	Route::GET('products/{idCategory}', 'Products\Api\ProductController@index')->name('api.products.index');
	Route::GET('products/product/{id}', 'Products\Api\ProductController@show')->name('api.products.show');
	Route::POST('products/','Products\Api\ProductController@store')->name('api.products.store');
        Route::PUT('products/product/{id}','Products\Api\ProductController@update')->name('api.products.update');
	Route::DELETE('products/product/{id}','Products\Api\ProductController@destroy')->name('api.products.destroy');
