<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Users extends Model {

    protected $table = "user";
    protected $fillable = [
        'customer_name',
        'customer_lastname',
        'customer_phone',
        'customer_address',
        'customer_email',
        'nickname',
        'actived',
        'customer_country_id',
        'customer_state_id',
        'customer_city_id',
        'password',
    ];

}
