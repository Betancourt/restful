<?php

/*******************************************************************
 * Quick Chance 
 * Login and Signup controller with RESTful API  
 * @author Leonardo Betancourt
 */

namespace App\Http\Controllers\State\Api;

use Validator;
use Illuminate\Http\Request;
use DB;
use App\Http\Controllers\Controller;
use Illuminate\Database\QueryException;

class StateController extends Controller {
    /*
     * Se deben de validar todos los campos que vienen de la peticion
     */

   public function valid($request) {
        //Validaciones
        $validator = Validator::make($request->all(), [
                    'id_state' => 'required|max:11',
                    'state_name' => 'required|max:80',
                    'actived' => 'required|max:4',
                    'id_country_state' =>'required|max:11'
        ]);

        return response(array(
            'error' => true,
            'status' => false,
            'message' => $validator->errors()->all(),
                ), 500);
    }

    /*
     * Login function 
     * @author Leonardo Betancourt 
     * (en desarrollo)
     * el campo de nickname o email , tendra el nombre definitivo de nickname 
     */

    public function getStateList($id_state_country) {
        try {

            $states = DB::select("select id_state,state_name from state where id_country_state=$id_state_country ");

            return response(array(
                'error' => false,
                'status' => true,
                'message' => 'states for id_country_state= '. $id_state_country,
                'data' => [
                    'States' => $states,
                ]
                    ), 200);
        } catch (QueryException $e) {

            return response(array(
                'error' => true,
                'status' => false,
                'message' => 'The server does not respond',
                    ), 500);
        }
    }
}
