<?php

/* * *****************************************************************
 * Quick Chance 
 * Login and Signup controller with RESTful API  
 * @author Leonardo Betancourt
 */

namespace App\Http\Controllers\Country\Api;

use Validator;
use Illuminate\Http\Request;
use DB;
use App\Http\Controllers\Controller;
use Illuminate\Database\QueryException;

class CountryController extends Controller {
    /*
     * Se deben de validar todos los campos que vienen de la peticion
     */

    public function valid($request) {
        //Validaciones
        $validator = Validator::make($request->all(), [
                    'id_country' => 'required|max:11',
                    'country_name' => 'required|max:80',
                    'actived' => 'required|max:4',
        ]);

        return response(array(
            'error' => true,
            'status' => false,
            'message' => $validator->errors()->all(),
                ), 500);
    }

    /*
     * Login function 
     * @author Leonardo Betancourt 
     * (en desarrollo)
     * el campo de nickname o email , tendra el nombre definitivo de nickname 
     */

    public function getCountryList() {
        try {

            $country = DB::select("select id_country,country_name from country where actived=1");

            return response(array(
                'error' => false,
                'status' => true,
                'message' => '',
                'data' => [
                    'countries' => $country,
                ]
                    ), 200);
        } catch (QueryException $e) {

            return response(array(
                'error' => true,
                'status' => false,
                'message' => 'The server does not respond',
                    ), 500);
        }
    }

}
