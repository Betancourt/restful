<?php

/*******************************************************************
 * Quick Chance
 * Login and Signup controller with RESTful API
 * @author Leonardo Betancourt
 */

namespace App\Http\Controllers\Users\Api;

use Validator;
use Illuminate\Http\Request;
use DB;
use App\Http\Controllers\Controller;
use Illuminate\Database\QueryException;

class UserController extends Controller
{
    /*
     * Se deben de validar todos los campos que vienen de la peticion
     */
    public function registerValidation($request)
    {
        //Validaciones
        $validator = Validator::make($request->all(), [
            'customer_name' => 'required|max:80',
            'customer_lastname' => 'required|max:80',
            'customer_phone' => 'required|max:45',
            'customer_address' => 'required|max:200',
            'customer_email' => 'required|max:140',
            'nickname' => 'required|max:50',
            'customer_country_id' => 'required|max:11',
            'customer_state_id' => 'required|max:11',
            'customer_city_id' => 'required|max:11',
            'password' => 'required|max:45',
            'user_type_id' => 'required|max:11',
            'type' => 'required|max:50',
        ]);

        return response(array(
            'error' => true,
            'status' => false,
            'message' => $validator->errors()->all(),
        ), 500);
    }

    public function facebookValidation($request)
    {
        //Validaciones
        $validator = Validator::make($request->all(), [
            'customer_name' => 'required|max:80',
            'customer_lastname' => 'required|max:80',
            'customer_phone' => 'required|max:45',
            'customer_address' => 'required|max:200',
            'customer_email' => 'required|max:140',
            'nickname' => 'required|max:50',
            'actived' => 'required|max:4',
            'customer_country_id' => 'required|max:11',
            'customer_state_id' => 'required|max:11',
            'customer_city_id' => 'required|max:11',
            'password' => 'required|max:45',
            'user_type_id' => 'required|max:11',
            'type' => 'required|max:50',
            'token' => 'required|max:300'
        ]);

        return response(array(
            'error' => true,
            'status' => false,
            'message' => $validator->errors()->all(),
        ), 500);
    }

    public function loginValidation($request)
    {
        //Validaciones
        $validator = Validator::make($request->all(), [
            'nickname' => 'required|max:50',
            'password' => 'required|max:45',
        ]);
        return response(array(
            'error' => true,
            'status' => false,
            'message' => $validator->errors()->all(),
        ), 500);
    }

    public function facebookLoginValidation($request)
    {
        //Validaciones
        $validator = Validator::make($request->all(), [
            'fb_id' => 'required',
            'token' => 'required',
        ]);
        return response(array(
            'error' => true,
            'status' => false,
            'message' => $validator->errors()->all(),
        ), 500);
    }

    /*
     * Login function 
     * @author Leonardo Betancourt 
     * (en desarrollo)
     * el campo de nickname o email , tendra el nombre definitivo de nickname 
     */

    public function login(Request $request)
    {
        // si la estructura es incorrecta
        if ($request->has('type') && $request->type !== 1) {
            if (!$request->has('nickname', 'password')) {
                return response(array(
                    'error' => true,
                    'status' => false,
                    'message' => 'Incorrect structure',
                ), 500);
            }
            //Validaciones
            $this->loginValidation($request);
        } else {
            if (!$request->has('fb_id', 'token')) {
                return response(array(
                    'error' => true,
                    'status' => false,
                    'message' => 'Incorrect structure',
                ), 500);
            }
            //Validaciones
            $this->facebookLoginValidation($request);
        }

        try {
            $user='';
            if ($request->has('type') && $request->type !== 1) {
                $user = DB::select("select * from user where (nickname = '$request->nickname' OR customer_email = '$request->nickname' ) and password = $request->password");
            } else {
                $user = DB::select("select * from user where (fb_id = '$request->fb_id')");
            }
            if (empty($user)) {
                return response(array(
                    'error' => true,
                    'status' => false,
                    'message' => 'Please verify user or password ', //Quitar el getMessage() al final por cuestiones de seguridad
                ), 404);
            } else {
                return response(array(
                    'error' => false,
                    'status' => true,
                    'message' => 'Login success!!',
                    'data' => [
                        'response' => $user,
                    ]
                ), 200);
            }
        } catch (QueryException $e) {
            return response(array(
                'error' => true,
                'status' => false,
                'message' => 'The server does not respond' . $e->getMessage(), //Quitar el getMessage() al final por cuestiones de seguridad
            ), 500);
        }
    }

    /**
     * Store a new user
     * @author Leonardo Betancourt Caicedo <leobetacai@gmail.com>
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function signup(Request $request)
    {
        // si la estructura es incorrecta

        if ($request->has('type') && $request->type === 1) {
            if (!$request->has('customer_name',
                'customer_lastname',
                'customer_country_id',
                'customer_state_id',
                'customer_city_id',
                'user_type_id',
                'actived',
                'token',
                'type',
                'fb_id'
            )) {
                return response(array(
                    'error' => true,
                    'status' => false,
                    'message' => 'Incorrect structure in method' . ' ' . 'Values in request format JSON = ' . json_encode($request->all()),
                ), 500);
            }
            //Validaciones
            $this->facebookValidation($request);
        } else {
            if (!$request->has('customer_name',
                'customer_lastname',
                'customer_phone',
                'customer_address',
                'customer_email',
                'nickname',
                'customer_country_id',
                'customer_state_id',
                'customer_city_id',
                'password',
                'user_type_id',
                'type'
            )) {
                return response(array(
                    'error' => true,
                    'status' => false,
                    'message' => 'Incorrect structure in method' . ' ' . 'Values in request format JSON = ' . json_encode($request->all()),
                ), 500);
            }
            //Validaciones
            $this->registerValidation($request);
        }

        try {
            if ($request->has('type') && $request->type !== 1) {
                //Validate nickname
                $v_nick = DB::select("select id_user from user where nickname ='$request->nickname'");
                //Validate email
                $v_email = DB::select("select id_user from user where customer_email ='$request->customer_email'");

                if (!empty($v_nick)) {
                    return response(array(
                        'error' => true,
                        'status' => true,
                        'message' => 'Duplicate!!',
                        'data' => [
                            'response' => 'Nickname taken ...please take other one',
                        ]
                    ), 409);
                }

                if (!empty($v_email)) {
                    return response(array(
                        'error' => true,
                        'status' => true,
                        'message' => 'Duplicate!!',
                        'data' => [
                            'response' => 'Email taken ...please take other one',
                        ]
                    ), 409);
                }
            } else {
                $v_fbId = DB::select("select id_user from user where fb_id ='$request->fb_id'");
                if (!empty($v_fbId)) {
                    return response(array(
                        'error' => true,
                        'status' => true,
                        'message' => 'Duplicate!!',
                        'data' => [
                            'response' => 'You have already registered, Please login',
                        ]
                    ), 409);
                }
            }

            /*
             * Save the register in database
             */
            if ($request->has('type') && $request->type !== 1) {
                $insertedId = DB::table('user')->insertGetId($request->all());
                if($insertedId){
                    return response(array(
                        'error' => false,
                        'status' => true,
                        'message' => 'Sucess!!',
                        'data' => [
                            'response' => 'User has been created successfully',
                        ]
                    ), 200);
                } else {
                    return response(array(
                        'error' => true,
                        'status' => true,
                        'message' => 'Duplicate!!',
                        'data' => [
                            'response' => 'Please try again!!!',
                        ]
                    ), 409);
                }
            } else {
                $insertedId = DB::table('user')->insertGetId($request->all());
                if($insertedId){
                    return response(array(
                        'error' => false,
                        'status' => true,
                        'message' => 'Sucess!!',
                        'data' => [
                            'response' => 'User has been created successfully',
                        ]
                    ), 200);
                } else {
                    return response(array(
                        'error' => true,
                        'status' => true,
                        'message' => 'Duplicate!!',
                        'data' => [
                            'response' => 'Please try again!!!',
                        ]
                    ), 409);
                }
            }
        } catch (QueryException $e) {
            return response(array(
                'error' => true,
                'status' => false,
                'message' => "Server error" . $e->getMessage(),
            ), 500);
        }
    }
}
