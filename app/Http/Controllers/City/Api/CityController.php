<?php

/*******************************************************************
 * Quick Chance 
 * Login and Signup controller with RESTful API  
 * @author Leonardo Betancourt
 */

namespace App\Http\Controllers\City\Api;

use Validator;
use Illuminate\Http\Request;
use DB;
use App\Http\Controllers\Controller;
use Illuminate\Database\QueryException;

class CityController extends Controller {
    /*
     * Se deben de validar todos los campos que vienen de la peticion
     */

   public function valid($request) {
        //Validaciones
        $validator = Validator::make($request->all(), [
                    'id_city' => 'required|max:11',
                    'city_name' => 'required|max:80',
                    'actived' => 'required|max:4',
                    'id_state_city' =>'required|max:11'
        ]);

        return response(array(
            'error' => true,
            'status' => false,
            'message' => $validator->errors()->all(),
                ), 500);
    }

    /*
     * Login function 
     * @author Leonardo Betancourt 
     * (en desarrollo)
     * el campo de nickname o email , tendra el nombre definitivo de nickname 
     */

    public function getCityList($id_city_state) {
        try {

            $cities = DB::select("select id_city,city_name from city where id_state_city=$id_city_state ");

            return response(array(
                'error' => false,
                'status' => true,
                'message' => 'Cities for id_state_city= '. $id_city_state,
                'data' => [
                    'Cities' => $cities,
                ]
                    ), 200);
        } catch (QueryException $e) {

            return response(array(
                'error' => true,
                'status' => false,
                'message' => 'The server does not respond',
                    ), 500);
        }
    }
}
